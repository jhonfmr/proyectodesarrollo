DROP TABLE IF EXISTS vehiculos_pertenecen CASCADE;
DROP TABLE IF EXISTS piezas_pertenecen CASCADE;
DROP TABLE IF EXISTS vehiculos_vendidos CASCADE;
DROP TABLE IF EXISTS ventas CASCADE;
DROP TABLE IF EXISTS piezas_orden_trabajo CASCADE;
DROP TABLE IF EXISTS vehiculos_cotizados CASCADE;
DROP TABLE IF EXISTS cotizaciones CASCADE;
DROP TABLE IF EXISTS piezas CASCADE;
DROP TABLE IF EXISTS vehiculos CASCADE;
DROP TABLE IF EXISTS ordenes_de_trabajo CASCADE;
DROP TABLE IF EXISTS sedes CASCADE;
DROP TABLE IF EXISTS empleados CASCADE;


CREATE TABLE empleados
(
nombre varchar(50) NOT NULL,
apellido varchar(50) NOT NULL,  
cedula_empleado varchar(50) PRIMARY KEY,
email varchar(50) NOT NULL,
direccion varchar(50) NOT NULL,
telefono varchar(15) NOT NULL,
cargo varchar(50) NOT NULL,
estado varchar(50) NOT NULL, 
usuario varchar(50) UNIQUE NOT NULL,  
contrasena varchar(50) NOT NULL,  
codigo_sede varchar(50)  
);

CREATE TABLE sedes
(
codigo_sede varchar(10) PRIMARY KEY, 
telefono varchar(10) NOT NULL, 
nombre varchar(50) UNIQUE NOT NULL,
direccion varchar(50) NOT NULL,
ciudad varchar(50) NOT NULL,
cedula_empleado varchar(50) NOT NULL 
);

CREATE TABLE vehiculos
(
codigo_vehiculo serial PRIMARY KEY,
marca varchar(50) NOT NULL,
modelo varchar(50) NOT NULL,
color varchar(50) NOT NULL,
precio float NOT NULL
);

CREATE TABLE piezas
(
codigo_pieza serial PRIMARY KEY,
nombre varchar(50) NOT NULL,
marca varchar(50) NOT NULL,
precio float NOT NULL
);

CREATE TABLE ordenes_de_trabajo
(
codigo_orden serial PRIMARY KEY,
cedula_cliente varchar(50) NOT NULL,
cedula_empleado varchar(50) NOT NULL REFERENCES empleados(cedula_empleado)
ON UPDATE CASCADE ON DELETE NO ACTION,
nombre_cliente varchar(50) NOT NULL,
descripcion varchar(500) NOT NULL,
placa varchar(10) NOT NULL,
modelo varchar(50) NOT NULL,
marca varchar(50) NOT NULL,
fecha_orden date NOT NULL,
valor_total integer NOT NULL,
estado  varchar(10) NOT NULL
);

CREATE TABLE ventas
(
codigo_ventas serial PRIMARY KEY,
cedula_cliente varchar(50) NOT NULL,
telefono_cliente varchar(50) NOT NULL,
precio_total_venta float NOT NULL,
metodo_de_pago varchar(50) NOT NULL,
fecha date NOT NULL,
cedula_empleado varchar(50) REFERENCES empleados (cedula_empleado)   
ON UPDATE CASCADE ON DELETE NO ACTION,
nombre_cliente varchar(50) NOT NULL
);

CREATE TABLE vehiculos_vendidos
(
codigo_ventas serial NOT NULL REFERENCES ventas (codigo_ventas)   
ON UPDATE CASCADE ON DELETE NO ACTION,
codigo_vehiculo serial NOT NULL REFERENCES vehiculos (codigo_vehiculo)   
ON UPDATE CASCADE ON DELETE NO ACTION,
cantidad integer NOT NULL,
precio_total float NOT NULL,
CONSTRAINT veh_vend_PK PRIMARY KEY (codigo_ventas,codigo_vehiculo)
);

CREATE TABLE cotizaciones
(
codigo_cotizacion serial PRIMARY KEY,
nombre_cliente varchar(50) NOT NULL,
cedula_cliente varchar(50)  NOT NULL,
cedula_empleado varchar(50) NOT NULL REFERENCES empleados (cedula_empleado)   
ON UPDATE CASCADE ON DELETE NO ACTION,
fecha date NOT NULL,
total_cotizacion float NOT NULL
);

CREATE TABLE vehiculos_cotizados
(
codigo_cotizacion serial NOT NULL REFERENCES cotizaciones (codigo_cotizacion) 
ON UPDATE CASCADE ON DELETE NO ACTION,
codigo_vehiculo serial NOT NULL REFERENCES vehiculos (codigo_vehiculo) 
ON UPDATE CASCADE ON DELETE NO ACTION,
cantidad integer NOT NULL,
total_veh_cot float NOT NULL,
CONSTRAINT veh_cot_pk PRIMARY KEY (codigo_cotizacion, codigo_vehiculo)
); 


CREATE TABLE piezas_orden_trabajo
(
codigo_orden serial NOT NULL REFERENCES ordenes_de_trabajo (codigo_orden)   
ON UPDATE CASCADE ON DELETE NO ACTION,
codigo_pieza serial NOT NULL REFERENCES piezas (codigo_pieza)
ON UPDATE CASCADE ON DELETE NO ACTION,
CONSTRAINT piezas_orden_pk PRIMARY KEY (codigo_orden,codigo_pieza),
cantidad integer not null,
precio_total_pieza float not null
);

CREATE TABLE vehiculos_pertenecen
(
codigo_vehiculo serial NOT NULL REFERENCES vehiculos(codigo_vehiculo) ON UPDATE CASCADE ON DELETE NO ACTION,
codigo_sede varchar(10) NOT NULL REFERENCES sedes(codigo_sede) ON UPDATE CASCADE ON DELETE NO ACTION,
cantidad integer not null,
CONSTRAINT veh_pert_pk PRIMARY KEY (codigo_vehiculo,codigo_sede)
);

CREATE TABLE piezas_pertenecen
(
codigo_pieza serial NOT NULL REFERENCES piezas(codigo_pieza) ON UPDATE CASCADE ON DELETE NO ACTION,
codigo_sede varchar(10) NOT NULL REFERENCES sedes(codigo_sede) ON UPDATE CASCADE ON DELETE NO ACTION,
cantidad integer not null,
CONSTRAINT pie_pert_pk PRIMARY KEY (codigo_pieza,codigo_sede)
);

INSERT INTO empleados VALUES ('sara','muñoz','1144094677','sara@gmail.com','carrera 26q #100-02','3157896522','gerente','activo','sara','gerente','');
INSERT INTO sedes VALUES ('s01','4532218','impu santander','Avenida 5N','bucaramanga','1144094677');

ALTER TABLE empleados ADD CONSTRAINT empl_fk FOREIGN KEY (codigo_sede) REFERENCES sedes(codigo_sede)      
ON UPDATE CASCADE ON DELETE NO ACTION; 

ALTER TABLE sedes ADD CONSTRAINT sede_fk FOREIGN KEY (cedula_empleado) REFERENCES empleados(cedula_empleado)
ON UPDATE CASCADE ON DELETE NO ACTION;

--EMPLEADOS
INSERT INTO empleados VALUES ('jhon','mena','1144090934','jhon@gmail.com','carrera 26Ñ #112-98','3104178924','jefe taller','activo','jhon','jefe taller','s01');
INSERT INTO empleados VALUES ('paola','muñoz','1458569783','paola@gmail.com','calle 13 con carrera 5','3219953321','vendedor','activo','paola','vendedor','s01');
INSERT INTO empleados VALUES ('jhonier','calero','1158642585','jhonier@gmail.com','calle 5 N #112-25','3215894715','vendedor','activo','jhonier','vendedor','s01');
INSERt INTO empleados VALUES ('carlos','martinez','11023698741','martinez@gmail.com','calle 26t #112-58','3158965742','vendedor','activo','carlos','vendedor','s01');
INSERT INTO empleados VALUES ('jose','fernandez','1512365987','jose@gmail.com','calle 20 con carrera 3S','3215210001','vendedor','activo','jose','vendedor','s01');
INSERT INTO empleados VALUES ('paulo','llanos','1158632655','paulo@gmail.com','calle 25g #112-25','3215854777','vendedor','activo','paulo','vendedor','s01');
INSERt INTO empleados VALUES ('pedro','rodriguez','1122036988','pedro@gmail.com','calle 26ñ #112-58','3158955874','vendedor','activo','pedro','vendedor','s01');

INSERT INTO empleados VALUES ('esteban','martinez','1158974265','esteban@gmail.com','calle 5 con carrera 20','3186988925','jefe taller','activo','esteban','jefe taller','s02');
INSERT INTO empleados VALUES ('carol','salazar','1122369748','carol@gmail.com','calle 72 con carrera 13','3105698745','vendedor','activo','carol','vendedor','s02');
INSERT INTO empleados VALUES ('suaza','agudelo','32158964','suaza@gmail.com','calle 26p con calle 9','3105987456','vendedor','activo','suaza','vendedor','s02');
INSERT INTO empleados VALUES ('james','perea','396548745','james@gmail.com','carrera 2K con calle 34','3116059878','vendedor','activo','james','vendedor','s02');
INSERT INTO empleados VALUES ('annie','muñoz','1145138848','annie@gmail.com','carrera 8 #23','3201569874','vendedor','activo','annie','vendedor','s02');
INSERT INTO empleados VALUES ('meneses','meneses','315698455','meneses@gmail.com','calle 75 con calle 99A','3115648799','vendedor','activo','meneses','vendedor','s02');
INSERT INTO empleados VALUES ('harold','martinez','1158963548','harold@gmail.com','carrera 89N con calle 34C','3125695555','vendedor','activo','harold','vendedor','s02');
INSERT INTO empleados VALUES ('maria','salazar','1145134548','maria@gmail.com','carrera 54 con calle 87H','3151569124','vendedor','activo','maria','vendedor','s02');

INSERT INTO empleados VALUES ('frederick','rodriguez','1144090655','frederick@gmail.com','carrera 26ñ #112-55','3117323983','jefe taller','activo','frederick','jefe taller','s03');
INSERT INTO empleados VALUES ('carolina','rojas','1188974566','carolina@gmail.com','carrera 25 con diagonal 5','3201569874','vendedor','activo','carolina','vendedor','s03');
INSERT INTO empleados VALUES ('fabio','agudelo','1187965','fabio@gmail.com','carrera 22 con diagonal 4','3156987741','vendedor','activo','fabio','vendedor','s03');
INSERT INTO empleados VALUES ('joan','manuel','1181274566','joan@gmail.com','carrera 4','31564648922','vendedor','activo','joan','vendedor','s03');
INSERT INTO empleados VALUES ('manuel','mena','5698741','manuel@gmail.com','carrera 10 con calle 20','3218799666','vendedor','activo','manuel','vendedor','s03');
INSERT INTO empleados VALUES ('monica','alejandra','1159658740','monica@gmail.com','carrera 35 con calle 1','3185256933','vendedor','activo','monica','vendedor','s03');
INSERT INTO empleados VALUES ('teresa','mejia','31879265','teresa@gmail.com','carrera 26p #98-115','3116011097','vendedor','activo','teresa','vendedor','s03');
INSERT INTO empleados VALUES ('shirley','useche','1158963100','shirley@gmail.com','carrera 5S con calle 26p','3189654781','vendedor','activo','shirley','vendedor','s03');

--SEDES
INSERT INTO sedes VALUES ('s02','4569314','impu cali','Carrera 8E #34-5','cali','1144094677');
INSERT INTO sedes VALUES ('s03','3584281','impu medellin','Avenida 3S con calle 5','medellin','1144094677');
--VEHICULOS
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('mazda','2006','gris',30000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('dodge','2000','rojo',20000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('mazda','2016','negro',45000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('nissan','2002','naranja',25000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('nissan','1996','negro',20000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('mazda','2013','rojo',28000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('ford','2016','verde',50000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('ford','2015','blanco',50000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('mazda','2004','rojo',13000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('nissan','2000','verde',21000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('chevrolet','1999','rojo',10000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('bmw','2016','verde',43000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('chevrolet','2006','blanco',33000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('bmw','2014','gris',30000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('mazda','2001','rojo',26000000);
INSERT INTO vehiculos (marca,modelo,color,precio) VALUES ('dodge','2016','verde',55000000);
--PIEZAS
INSERT INTO piezas (nombre,marca,precio) VALUES ('llantas','rapifluz',50000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('sistema de encendico/incandecencia','batter',95000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('bolsa de aire','automax',110000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('sistema de escape','automax',225000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('refrigeracion','automax',556000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('bombillas','bomblix',75000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('bateria de automovil','Batter',250000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('volante de direccion','automax',135000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('carburador','motocar',295000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('bocina','automax',50000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('caja de cambios','automax',335000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('amortiguador','automax',100000);
INSERT INTO piezas (nombre,marca,precio) VALUES ('acelerador','motocar',75000);


---vehiculos en sede
INSERT INTO vehiculos_pertenecen VALUES('1', 's01', 10);
INSERT INTO vehiculos_pertenecen VALUES('3', 's01', 10);
INSERT INTO vehiculos_pertenecen VALUES('4', 's01', 8);
INSERT INTO vehiculos_pertenecen VALUES('5', 's01', 7);
INSERT INTO vehiculos_pertenecen VALUES('6', 's01', 10);
INSERT INTO vehiculos_pertenecen VALUES('7', 's01', 10);
INSERT INTO vehiculos_pertenecen VALUES('8', 's01', 10);
INSERT INTO vehiculos_pertenecen VALUES('9', 's01', 7);
INSERT INTO vehiculos_pertenecen VALUES('12', 's01', 10);
INSERT INTO vehiculos_pertenecen VALUES('15', 's01', 8);

INSERT INTO vehiculos_pertenecen VALUES('1', 's02', 10);
INSERT INTO vehiculos_pertenecen VALUES('4', 's02', 2);
INSERT INTO vehiculos_pertenecen VALUES('5', 's02', 5);
INSERT INTO vehiculos_pertenecen VALUES('6', 's02', 8);
INSERT INTO vehiculos_pertenecen VALUES('7', 's02', 20);
INSERT INTO vehiculos_pertenecen VALUES('10', 's02', 5);
INSERT INTO vehiculos_pertenecen VALUES('16', 's02', 7);
INSERT INTO vehiculos_pertenecen VALUES('13', 's02', 3);

INSERT INTO vehiculos_pertenecen VALUES('1', 's03', 10);
INSERT INTO vehiculos_pertenecen VALUES('2', 's03', 2);
INSERT INTO vehiculos_pertenecen VALUES('3', 's03', 5);
INSERT INTO vehiculos_pertenecen VALUES('4', 's03', 8);
INSERT INTO vehiculos_pertenecen VALUES('5', 's03', 20);
INSERT INTO vehiculos_pertenecen VALUES('6', 's03', 5);
INSERT INTO vehiculos_pertenecen VALUES('7', 's03', 7);
INSERT INTO vehiculos_pertenecen VALUES('8', 's03', 3);
INSERT INTO vehiculos_pertenecen VALUES('9', 's03', 15);
INSERT INTO vehiculos_pertenecen VALUES('10', 's03', 6);
INSERT INTO vehiculos_pertenecen VALUES('11', 's03', 4);



--piezas en sede
INSERT INTO piezas_pertenecen VALUES('1', 's01', 8);
INSERT INTO piezas_pertenecen VALUES('2', 's01', 20);
INSERT INTO piezas_pertenecen VALUES('3', 's01', 8);
INSERT INTO piezas_pertenecen VALUES('4', 's01', 5);
INSERT INTO piezas_pertenecen VALUES('5', 's01', 7);
INSERT INTO piezas_pertenecen VALUES('6', 's01', 11);
INSERT INTO piezas_pertenecen VALUES('7', 's01', 16);
INSERT INTO piezas_pertenecen VALUES('8', 's01', 8);
INSERT INTO piezas_pertenecen VALUES('10', 's01', 10);
INSERT INTO piezas_pertenecen VALUES('11', 's01', 10);


INSERT INTO piezas_pertenecen VALUES('1', 's02', 17);
INSERT INTO piezas_pertenecen VALUES('2', 's02', 9);
INSERT INTO piezas_pertenecen VALUES('3', 's02', 10);
INSERT INTO piezas_pertenecen VALUES('5', 's02', 18);
INSERT INTO piezas_pertenecen VALUES('7', 's02', 10);
INSERT INTO piezas_pertenecen VALUES('9', 's02', 10);
INSERT INTO piezas_pertenecen VALUES('10', 's02', 10);
INSERT INTO piezas_pertenecen VALUES('12', 's02', 15);
INSERT INTO piezas_pertenecen VALUES('13', 's02', 20);

INSERT INTO piezas_pertenecen VALUES('2', 's03', 17);
INSERT INTO piezas_pertenecen VALUES('5', 's03', 9);
INSERT INTO piezas_pertenecen VALUES('7', 's03', 10);
INSERT INTO piezas_pertenecen VALUES('8', 's03', 20);
INSERT INTO piezas_pertenecen VALUES('9', 's03', 18);
INSERT INTO piezas_pertenecen VALUES('10', 's03', 10);
INSERT INTO piezas_pertenecen VALUES('11', 's03', 10);
INSERT INTO piezas_pertenecen VALUES('12', 's03', 10);
INSERT INTO piezas_pertenecen VALUES('13', 's03', 15);


--ordenes de trabajo
INSERT INTO ordenes_de_trabajo (cedula_cliente,cedula_empleado,nombre_cliente,descripcion,placa,modelo,marca,fecha_orden,valor_total,estado) 
VALUES('1124789476','1144090934','juan','cambio de aceite','ahj 990','2006','nissan','2016-11-05',100000,'pendiente');
INSERT INTO ordenes_de_trabajo (cedula_cliente,cedula_empleado,nombre_cliente,descripcion,placa,modelo,marca,fecha_orden,valor_total,estado) 
VALUES('1189878490','1144090934','jose','cambio de las 3 llantas','ahj 990','1998','mazda','2016-11-05',270000,'pendiente');
INSERT INTO ordenes_de_trabajo (cedula_cliente,cedula_empleado,nombre_cliente,descripcion,placa,modelo,marca,fecha_orden,valor_total,estado) 
VALUES('1189878490','1158974265','maria','cambio de 4 bombillas','ahj 990','1998','mazda','2016-11-15',300000,'pendiente');


--piezas orden
INSERT INTO piezas_orden_trabajo VALUES (1,1,2,100000);
INSERT INTO piezas_orden_trabajo VALUES (1,2,4,70000);
INSERT INTO piezas_orden_trabajo VALUES (3,6,4,300000);

--cotizaciones
INSERT INTO cotizaciones (nombre_cliente,cedula_cliente,cedula_empleado,fecha,total_cotizacion)
VALUES ('ana','1127798990','1458569783','2016-11-11',61000000);
INSERT INTO cotizaciones (nombre_cliente,cedula_cliente,cedula_empleado,fecha,total_cotizacion)
VALUES ('pedro','318695541','315698455','2016-11-15',50000000);
INSERT INTO cotizaciones (nombre_cliente,cedula_cliente,cedula_empleado,fecha,total_cotizacion)
VALUES ('camila','1127798990','1187965','2016-11-22',20000000);

--vehiculos cotizados
INSERT INTO vehiculos_cotizados VALUES (1,1,2,60000000);
INSERT INTO vehiculos_cotizados VALUES (1,4,2,1000000);
INSERT INTO vehiculos_cotizados VALUES (2,7,1,50000000);
INSERT INTO vehiculos_cotizados VALUES (3,11,2,20000000);

--ventas

INSERT INTO ventas (cedula_cliente, telefono_cliente, precio_total_venta, metodo_de_pago, fecha, cedula_empleado, nombre_cliente) 
VALUES ('12345678', '3185289077' ,148480000, 'Efectivo', '2016-12-11','1458569783', 'Jose Ramirez');
INSERT INTO ventas  (cedula_cliente, telefono_cliente, precio_total_venta, metodo_de_pago, fecha, cedula_empleado, nombre_cliente) 
VALUES ('87654321', '3188543111' ,46400000, 'Tarjeta', '2016-12-11','1458569783', 'Maria Perez');
INSERT INTO ventas (cedula_cliente, telefono_cliente, precio_total_venta, metodo_de_pago, fecha, cedula_empleado, nombre_cliente) 
VALUES ('13248756', '3178844213' , 104400000, 'Cheque', '2016-12-11','1458569783', 'Luis Muñoz');

INSERT INTO ventas (cedula_cliente, telefono_cliente, precio_total_venta, metodo_de_pago, fecha, cedula_empleado, nombre_cliente) 
VALUES ('88964231', '315477533' , 23200000, 'Cheque', '2016-12-11','1181274566', 'Ximena Llanos');
INSERT INTO ventas (cedula_cliente, telefono_cliente, precio_total_venta, metodo_de_pago, fecha, cedula_empleado, nombre_cliente) 
VALUES ('12135436', '313225021' , 145000000, 'Cheque', '2016-12-11','1181274566', 'Pablo Rodriguez');
INSERT INTO ventas  (cedula_cliente, telefono_cliente, precio_total_venta, metodo_de_pago, fecha, cedula_empleado, nombre_cliente) 
VALUES ('54712331', '320154154' , 78880000, 'Tarjeta', '2016-12-11','1181274566', 'Jhonier Calero');

INSERT INTO ventas (cedula_cliente, telefono_cliente, precio_total_venta, metodo_de_pago, fecha, cedula_empleado, nombre_cliente)  
VALUES ('88869203', '321654987' , 174000000, 'Tarjeta', '2016-12-11','1145138848', 'Jhon Mena');
INSERT INTO ventas (cedula_cliente, telefono_cliente, precio_total_venta, metodo_de_pago, fecha, cedula_empleado, nombre_cliente) 
VALUES ('7775213', '318754123' , 372360000, 'Cheque', '2016-12-11','1145138848', 'Laura Montes');
INSERT INTO ventas (cedula_cliente, telefono_cliente, precio_total_venta, metodo_de_pago, fecha, cedula_empleado, nombre_cliente) 
VALUES ('114408302', '317458963' , 99760000, 'Efectivo', '2016-12-11','1145138848', 'Nataly Hoyos');

-- vehiculos vendidos
INSERT INTO vehiculos_vendidos VALUES(1, 7, 2, 100000000);
INSERT INTO vehiculos_vendidos VALUES(1, 6, 1, 28000000);
INSERT INTO vehiculos_vendidos VALUES(2, 5, 2, 40000000);
INSERT INTO vehiculos_vendidos VALUES(3, 1, 3, 90000000);

INSERT INTO vehiculos_vendidos VALUES(4, 11, 2, 20000000);
INSERT INTO vehiculos_vendidos VALUES(5, 8, 2, 100000000);
INSERT INTO vehiculos_vendidos VALUES(5, 4, 1, 40000000);
INSERT INTO vehiculos_vendidos VALUES(6, 6, 1, 28000000);

INSERT INTO vehiculos_vendidos VALUES(7, 7, 3, 150000000);
INSERT INTO vehiculos_vendidos VALUES(8, 13, 2, 66000000);
INSERT INTO vehiculos_vendidos VALUES(8, 16, 1, 55000000);
INSERT INTO vehiculos_vendidos VALUES(8, 7, 4, 200000000);
INSERT INTO vehiculos_vendidos VALUES(9, 6, 2, 56000000);
INSERT INTO vehiculos_vendidos VALUES(9, 1, 1, 30000000);
