/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_desarrollo.Gerente;
import java.awt.Toolkit;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import javax.swing.*;
import proyecto_desarrollo.AccesoDatos;
import proyecto_desarrollo.Login;
/**
 *
 * @author invitado
 */
public class Inicio_Gerente extends javax.swing.JFrame 
{
   ManejadoraG maneja;
   static String cedula;
  
    /**
     * Creates new form incio
     */
    public Inicio_Gerente(String ced) {
        initComponents();
        cedula = ced;
        
        maneja = new ManejadoraG();
        
        //Se cambia el icono de la aplicacion
        Toolkit tool = Toolkit.getDefaultToolkit();
        setIconImage(tool.getImage(getClass().getResource("/img/icono.jpg")));
        
        
        //Posicion del componente realtiva a la pantalla, en este caso en el centro
        setLocationRelativeTo(null);
        //Instancia de la clase manejadora
        maneja = new ManejadoraG();
        
        /* Escuchas del ActionListener */
        btn_GestionUsuarioG.addActionListener(maneja);
        btn_GestionarSedeG.addActionListener(maneja);
        btn_GenerarReporte.addActionListener(maneja);
        btn_GestionarInventarioG.addActionListener(maneja);
        btn_logoutG.addActionListener(maneja);
        
        /* Escuchas del mouseListener */
        btn_GestionUsuarioG.addMouseListener(maneja);
        btn_GestionarSedeG.addMouseListener(maneja);
        btn_GenerarReporte.addMouseListener(maneja);
        btn_GestionarInventarioG.addMouseListener(maneja);
        /* Adicion de las escuchas WindowsListener */
        addWindowListener(maneja);
        

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_cargoGOTJ = new javax.swing.JLabel();
        pnl_Principal = new javax.swing.JPanel();
        btn_GestionarSedeG = new javax.swing.JButton();
        btn_GestionUsuarioG = new javax.swing.JButton();
        btn_GestionarInventarioG = new javax.swing.JButton();
        btn_GenerarReporte = new javax.swing.JButton();
        lbl_userGOTJ = new javax.swing.JLabel();
        lbl_nombreIG = new javax.swing.JLabel();
        lbl_cargoIG = new javax.swing.JLabel();
        lbl_Banner = new javax.swing.JLabel();
        btn_logoutG = new javax.swing.JButton();

        lbl_cargoGOTJ.setForeground(java.awt.Color.white);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(680, 560));
        setResizable(false);
        getContentPane().setLayout(null);

        pnl_Principal.setBackground(new java.awt.Color(255, 255, 255));
        pnl_Principal.setLayout(null);

        btn_GestionarSedeG.setBackground(new java.awt.Color(254, 254, 254));
        btn_GestionarSedeG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/gestionarSede.png"))); // NOI18N
        pnl_Principal.add(btn_GestionarSedeG);
        btn_GestionarSedeG.setBounds(360, 200, 140, 140);

        btn_GestionUsuarioG.setBackground(new java.awt.Color(255, 255, 255));
        btn_GestionUsuarioG.setForeground(new java.awt.Color(0, 0, 255));
        btn_GestionUsuarioG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/gestionarUsuario.png"))); // NOI18N
        pnl_Principal.add(btn_GestionUsuarioG);
        btn_GestionUsuarioG.setBounds(170, 200, 140, 140);

        btn_GestionarInventarioG.setBackground(new java.awt.Color(254, 254, 254));
        btn_GestionarInventarioG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/gestionarInventario.png"))); // NOI18N
        pnl_Principal.add(btn_GestionarInventarioG);
        btn_GestionarInventarioG.setBounds(170, 360, 140, 140);

        btn_GenerarReporte.setBackground(new java.awt.Color(254, 254, 254));
        btn_GenerarReporte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bGenerarReporte.png"))); // NOI18N
        pnl_Principal.add(btn_GenerarReporte);
        btn_GenerarReporte.setBounds(360, 360, 140, 140);

        lbl_userGOTJ.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/usuario.png"))); // NOI18N
        pnl_Principal.add(lbl_userGOTJ);
        lbl_userGOTJ.setBounds(450, 30, 60, 60);

        lbl_nombreIG.setForeground(java.awt.Color.white);
        lbl_nombreIG.setText("Nombre");
        pnl_Principal.add(lbl_nombreIG);
        lbl_nombreIG.setBounds(530, 30, 180, 20);

        lbl_cargoIG.setForeground(java.awt.Color.white);
        lbl_cargoIG.setText("Cargo");
        pnl_Principal.add(lbl_cargoIG);
        lbl_cargoIG.setBounds(530, 60, 180, 20);

        lbl_Banner.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/final.png"))); // NOI18N
        lbl_Banner.setText("dsfdadsf");
        pnl_Principal.add(lbl_Banner);
        lbl_Banner.setBounds(0, 0, 680, 130);

        btn_logoutG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/logout.png"))); // NOI18N
        pnl_Principal.add(btn_logoutG);
        btn_logoutG.setBounds(20, 150, 50, 50);

        getContentPane().add(pnl_Principal);
        pnl_Principal.setBounds(0, 0, 680, 540);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
       

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inicio_Gerente(cedula).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_GenerarReporte;
    private javax.swing.JButton btn_GestionUsuarioG;
    private javax.swing.JButton btn_GestionarInventarioG;
    private javax.swing.JButton btn_GestionarSedeG;
    private javax.swing.JButton btn_logoutG;
    private javax.swing.JLabel lbl_Banner;
    private javax.swing.JLabel lbl_cargoGOTJ;
    private javax.swing.JLabel lbl_cargoIG;
    private javax.swing.JLabel lbl_nombreIG;
    private javax.swing.JLabel lbl_userGOTJ;
    private javax.swing.JPanel pnl_Principal;
    // End of variables declaration//GEN-END:variables




    /*
    Clase interna que maneja cada uno de los eventos de los diferentes componentes
    */
    public class ManejadoraG implements ActionListener, MouseListener, WindowListener{

        AccesoDatos accesoDatos = new AccesoDatos();
        
        @Override
        public void actionPerformed(ActionEvent e) 
        {
              if (e.getSource() == btn_GestionUsuarioG) 
              {
                  GestionarUsuarioGerente interfaz = new GestionarUsuarioGerente(cedula);
                  interfaz.setVisible(true);
                  dispose();

              }else if(e.getSource() == btn_GestionarSedeG)
              {
                  GestionarSedesGerente interfaz = new GestionarSedesGerente(cedula);
                  interfaz.setVisible(true);
                  dispose();
                  
              }else if(e.getSource() == btn_GestionarInventarioG)
              {
                  GestionarInventarioGerente interfaz = new GestionarInventarioGerente(cedula);
                  interfaz.setVisible(true);
                  dispose();
                  
              }else if(e.getSource() == btn_GenerarReporte)
              {
                ReportesGerente interfaz = new ReportesGerente(cedula);
                  interfaz.setVisible(true);
                  dispose();
                  
              }else if(e.getSource() == btn_logoutG)
              {
                  int decision = JOptionPane.showConfirmDialog(null, "¿Desea cerrar la sesión?",
                          "Mensaje", JOptionPane.YES_NO_OPTION);
                 
                  if(decision == JOptionPane.YES_OPTION)
                  {
                      Login inicio = new Login();
                      inicio.setVisible(true);
                      dispose();
                  }
                  
              }

        }

        @Override
        public void mouseClicked(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) 
        {
             /*
            Cambio del icono asociado al boton cuando el cursor pasa por el boton
            */
            if(e.getSource() == btn_GestionUsuarioG)
            {
                btn_GestionUsuarioG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/gestionarUsuario2.png")));
            }else if(e.getSource() == btn_GestionarSedeG)
            {
                btn_GestionarSedeG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/gestionarSede2.png")));
            }else if(e.getSource() == btn_GestionarInventarioG)
            {
                btn_GestionarInventarioG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/gestionarInventario2.png")));
            }else if(e.getSource() == btn_GenerarReporte)
            {
                btn_GenerarReporte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bGenerarReporte2.png")));
            }
            
        }

        @Override
        public void mouseExited(MouseEvent e)
        {
            /*
            Cambio del icono asociado al boton cuando el cursor esta por fuera del boton
            */
            if(e.getSource() == btn_GestionUsuarioG){
                btn_GestionUsuarioG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/gestionarUsuario.png")));

            }else if(e.getSource() == btn_GestionarSedeG){
                btn_GestionarSedeG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/gestionarSede.png")));
            }else if(e.getSource() == btn_GestionarInventarioG){
                btn_GestionarInventarioG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/gestionarInventario.png")));
            }else if(e.getSource() == btn_GenerarReporte){
                btn_GenerarReporte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bGenerarReporte.png")));
            }
        }

        @Override
        public void windowOpened(WindowEvent e) {
           ResultSet respuesta = accesoDatos.peticionInformacionInicioGerente(cedula);
            try
            {
                respuesta.next();
                lbl_nombreIG.setText(respuesta.getString(1).toUpperCase() + " " + respuesta.getString(2).toUpperCase());
                lbl_cargoIG.setText(respuesta.getString(3).toUpperCase());
                
                
            }catch(SQLException sqlex)
            {
                JOptionPane.showMessageDialog(null,"Error al cargar la informacion de inicio del usuario",
                        "mensaje",JOptionPane.WARNING_MESSAGE);
            }
        }

        @Override
        public void windowClosing(WindowEvent e) {
            
        }

        @Override
        public void windowClosed(WindowEvent e) {
            
        }

        @Override
        public void windowIconified(WindowEvent e) {
            
        }

        @Override
        public void windowDeiconified(WindowEvent e) {
            
        }

        @Override
        public void windowActivated(WindowEvent e) {
            
        }

        @Override
        public void windowDeactivated(WindowEvent e) {
        }
        
    }

}

