/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_desarrollo;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.util.Calendar;
import java.util.GregorianCalendar;
        

/**
 *
 * @author saracmh
 */
public class AccesoDatos {
    Fachada fachada = new Fachada();
    ResultSet respuesta;
    Statement instruccion;
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param usuario
     * @param contraseña
     * @return String
     * Funcion que busca un empleado en la base de datos 
     * y retorna un String dependiendo del resultado
     */
    public String verificarUsuario (String usuario, String contraseña)
    {
        String orden = "SELECT cargo,contrasena FROM empleados WHERE usuario = '" + usuario + "';";
        
    //resultado de la consulta
     try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al consultar los datos de la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
     //verificar si la contraseña es correcta
     try
     {
         String contraseñaBd="";
            
         if(!respuesta.next())
         {
            return "noExiste";
         }else
            { 
                contraseñaBd = respuesta.getString("contrasena");
                if (contraseñaBd.equals(contraseña))
                    {
                        return respuesta.getString("cargo");
                    }else
                        {
                        return "contraseñaIncorrecta";
                        }
            }
     }catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null,"Error inesperado: " + ex,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
     return "Error";
    }
    
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param ComboBox cargo
     * @return void
     * Funcion que añade los cargos a un ComboBox
     */
    
    public void cargarCargo(JComboBox cargo)
    {
        cargo.addItem("gerente");
        cargo.addItem("jefe taller");
        cargo.addItem("vendedor");
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param ComboBox estado
     * @return void
     * Funcion que añade los estados a un ComboBox
     */
    public void cargarEstado(JComboBox estado)
    {
        estado.addItem("activo");
        estado.addItem("inactivo");
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param informacion
     * @param ComboBox sede
     * @return void
     * Funcion que añade los nombres de las sedes a un ComboBox
     */
    public void cargarSede(ResultSet informacion,JComboBox sede)
    {
     try
     {
         if(!informacion.next())
         {
            
         }else
            {
            sede.addItem(informacion.getString("nombre"));
            while(informacion.next())
            {
                sede.addItem(informacion.getString("nombre"));
            }
            }
     }catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null,"Error inesperado: " + ex,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param nombre
     * @param marca
     * @param precio
     * @return boolean
     * Funcion que registra una pieza en la base de datos
     */
    public boolean registrarPieza(String nombre, String marca, int precio)
    {
        
        boolean resultado=false;
        
        
        String orden = "SELECT * FROM piezas WHERE nombre='"+nombre+"' AND marca = '"+marca+"';";
        
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT 
            if(instruccion.executeQuery(orden).next())
            {
                resultado=true;
            }
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }

        if(resultado)
        {
            int decision = JOptionPane.showConfirmDialog(null, "la pieza ya existe,¿Desea actualizar el precio?",
                          "Mensaje", JOptionPane.YES_NO_OPTION);
                 
                  if(decision == JOptionPane.YES_OPTION)
                  {
                      orden = "UPDATE piezas SET precio = "+precio+" WHERE nombre ='"+nombre+"' AND marca = '"+marca+"';";
        
                    conexion = fachada.conectar();
                    try {
                        //Permite enviar instrucciones a la BD
                        instruccion =  conexion.createStatement();
                        //Se ejecuta la instruccion SQL de tipo UPDATE 
                        int actualizadas= instruccion.executeUpdate(orden);
                        resultado=true;
                        //Se cierra la conexion
                        fachada.cerrarConexion(conexion);
                        
                    } catch (SQLException sqlex)
                    {
                       JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                                "Mensaje",JOptionPane.ERROR_MESSAGE);
                    }
                  }else
                  {
                      return false;
                  }
        }else
        {
            orden = "INSERT INTO piezas (nombre,marca,precio) VALUES ('"+nombre+"','"+marca+"',"+precio+");";
        
        conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = !instruccion.execute(orden);
            JOptionPane.showMessageDialog(null,resultado);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        }
   
        return resultado;
        
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param nombre
     * @param apellido
     * @param cedula
     * @param email
     * @param direccion
     * @param cargo
     * @param sede
     * @param estado
     * @param usuario
     * @return boolean
     * Funcion que añade un usuario a la base de datos y retorna un boleano
     * dependiendo de si la operacion fue exitosa o no
     */
    
    public boolean peticionAñadirUsuario(String nombre,String apellido,String cedula,String email,String direccion,String telefono,String cargo,String estado,String usuario,String sede)
    {
        //nombre,apellido,cedula,direccion,cargo,sede,estado,usuario
                
        
        String orden = "INSERT INTO empleados VALUES ('"+nombre+"','"+apellido+"','"+cedula+"','"+email+"','"+direccion+"','"+telefono+"','"+cargo+"','"+estado+"','"+usuario+"','12345','"+sede+"');";
        boolean respuesta=false;
        
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = !instruccion.execute(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException sqlex)
        {
            if(sqlex.getErrorCode() == 0)
            {
                JOptionPane.showMessageDialog(null,"Ya se tiene un usuario con la cedula o la cuenta: ",
                    "Mensaje",JOptionPane.WARNING_MESSAGE);
            }else
            {
                JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL: " +sqlex,
                        "Mensaje",JOptionPane.ERROR_MESSAGE);
            }
            
        }
         return respuesta;
    }
     
    
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param mes
     * @param año
     * @return ResultSet
     * Funcion que reotrna informacion para el reporte desde
     * la tabla ordenes_de_trabajo
     */
    public ResultSet peticionReporteOrdenes(int mes, String año)
    {
        //nombre,apellido,cedula,direccion,cargo,sede,estado,usuario
                
        
        String orden = "SELECT codigo_orden,nombre_cliente,cedula_empleado,placa,valor_total,estado FROM ordenes_de_trabajo WHERE EXTRACT(YEAR FROM fecha_orden)="+año+"AND EXTRACT (MONTH FROM fecha_orden)="+mes;
        
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al consultar los datos a la tabla ordenes_de_trabajo: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param ComboBox marcas
     * @return void
     * Funcion que carga y extrae las marcas de las piezas de la base de datos
     */
    public void cargarMarcasGeneral(JComboBox marcas)
    {
        String orden = "SELECT DISTINCT marca FROM piezas;";
        
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        try
        {
            if(!respuesta.next())
            {
                marcas.addItem("error");
                marcas.setSelectedItem("error");
            }else
                {
                    marcas.addItem(respuesta.getString("marca"));
                    while(respuesta.next())
                    {
                        marcas.addItem(respuesta.getString("marca"));
                    }
                }
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param ComboBox nombres
     * @param marca
     * @return void
     * Funcion que carga y extrae los nombres de las piezas
     * segun su marca
     */
    
    public void cargarNombresMarca(JComboBox nombres,String marca)
    {
        String orden = "SELECT * FROM piezas WHERE marca='"+marca+"';";
        
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        try
        {
            if(!respuesta.next())
            {
                nombres.addItem("error");
                nombres.setSelectedItem("error");
            }else
                {
                    nombres.addItem(respuesta.getString("nombre"));
                    while(respuesta.next())
                    {
                        nombres.addItem(respuesta.getString("nombre"));
                    }
                }
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param sede
     * @param nombre
     * @param marca
     * @param cantidad
     * @return boolean
     * funcion que se encarga de añadir existencias de una pieza
     * al inventario, verifica si la pieza existe y añade a la 
     * linea correspondiente la cantidad ingresada
     * o en su defecto crea una nueva linea con la
     * dicha cantidad
     */
    
    public void peticionAñadirPieza(String sede,String nombre,String marca,int cantidad)
    {
        ResultSet consultaCodigoPieza,consultaCodigoSede,consultaPiezasExistentes=null;
        String codigoPieza="",codigoSede="";
        
        
        try
        {
        
        consultaCodigoPieza = peticionBuscarPieza(nombre,marca);
        consultaCodigoPieza.next();
        consultaCodigoSede = peticionBuscarCodigoSede(sede);
        consultaCodigoSede.next();
        
        codigoPieza=consultaCodigoPieza.getString("codigo_pieza"); 
        codigoSede=consultaCodigoSede.getString("codigo_sede");
        String revisar = "SELECT * FROM piezas_pertenecen WHERE codigo_pieza='"+codigoPieza+"' AND codigo_sede ='"+codigoSede+"';";
            
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            consultaPiezasExistentes=instruccion.executeQuery(revisar);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        try
        {
            if(consultaPiezasExistentes.next())
            {
                int nueva_cantidad=consultaPiezasExistentes.getInt("cantidad");
                nueva_cantidad=nueva_cantidad+cantidad;
                String orden = "UPDATE piezas_pertenecen SET cantidad="+nueva_cantidad+" WHERE codigo_pieza='"+codigoPieza+"' AND codigo_sede ='"+codigoSede+"';";
        
                try
                {
                    //Se hace la conexion a la base de datos
                    Connection conexion = fachada.conectar();
                    //Se ejecuta la instruccion SQL
                    instruccion = conexion.createStatement();
                    //Se obtienen los datos devueltos de la consulta 
                    instruccion.executeUpdate(orden);
                    //Se cierra la conexion con la base de datos
                    fachada.cerrarConexion(conexion);
            
                }catch(SQLException sqle)
                {
                    JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla piezas_pertenecen: " + sqle,
                            "Error",JOptionPane.ERROR_MESSAGE);
                }catch(NullPointerException e)
                {
                    JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                            "Error",JOptionPane.ERROR_MESSAGE);
                }
        
            }else
                {
                    String orden = "INSERT INTO piezas_pertenecen (codigo_pieza,codigo_sede,cantidad) VALUES ('"+codigoPieza+"','"+codigoSede+"',"+cantidad+");";
        
                    try
                    {
                        //Se hace la conexion a la base de datos
                        Connection conexion = fachada.conectar();
                        //Se ejecuta la instruccion SQL
                        instruccion = conexion.createStatement();
                        //Se obtienen los datos devueltos de la consulta 
                        instruccion.execute(orden);
                        //Se cierra la conexion con la base de datos
                        fachada.cerrarConexion(conexion);
            
                    }catch(SQLException sqle)
                    {
                        JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                                "Error",JOptionPane.ERROR_MESSAGE);
                    }catch(NullPointerException e)
                    {
                        JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                                "Error",JOptionPane.ERROR_MESSAGE);
                    }
                }
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param nombre
     * @param marca
     * @return boolean
     * Funcion que carga la informacion de una pieza
     * segun el nombre y la marca
     */
    
    public ResultSet peticionBuscarPieza(String nombre,String marca)
    {
        
        
        String orden = "SELECT * FROM piezas WHERE nombre='"+nombre+"'AND marca ='"+marca+"';";
             
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param nombre
     * @return boolean
     * Funcion que carga la informacion de una sede especifica
     */
    
    public ResultSet peticionBuscarCodigoSede(String nombre)
    {
        String orden = "SELECT * FROM sedes WHERE nombre ='"+nombre+"';";
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param cedula
     * @param direccion
     * @param email
     * @param telefono
     * @param cargo
     * @param estado
     * @paramsede
     * @return boolean
     * Funcion que se encarga de modificar la informacion de un usuario
     * previamente buscado por su cedula
     */
    public boolean peticionModificarUsuario(String cedula,String direccion,String email,String telefono,String cargo,String estado,String sede)
    {
        
        boolean respuesta=false;
        String orden = "UPDATE empleados SET direccion = '"+direccion+"',email ='"+email+/*"',telefono = '"+telefono+*/"',cargo = '"+cargo+"',estado = '"+estado+"',codigo_sede = '"+sede+"' WHERE cedula_empleado = '"+cedula+"';";
             
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.execute(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
        
       
    }
    
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param cedula
     * @return ResultSet
     * Funcion que carga la informacion de un usuario segun su cedula
     */
    public ResultSet peticionBuscarCedula(String cedula)
    {
        String orden = "SELECT * FROM empleados WHERE cedula_empleado ='"+cedula+"';";
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al ingresar los datos a la tabla usuarios: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param datos
     * @param direccion
     * @param telefono
     * @param cargo
     * @param estado
     * @param sede
     * @return void
     * Funcion que se encarga de actualizar la informacion de un usuario
     */
    public void actualizarModificarUsuarioGerente(ResultSet datos,JTextField direccion,JTextField email,JTextField telefono,JComboBox cargo,JComboBox estado,JComboBox sede)
    {
        
        String cargoUsuario,estadoUsuario,sedeUsuario;
        try
     {
         if(!datos.next())
         {
             direccion.setText("error");
             email.setText("error");
             telefono.setText("error");
             cargo.addItem("error");
             cargo.setSelectedItem("error");
             estado.addItem("error");
             estado.setSelectedItem("error");
             sede.addItem("error");
             sede.addItem("error");
             sede.setSelectedItem("error");
            
         }else
            {
            direccion.setText(datos.getString("direccion"));
            email.setText(datos.getString("email"));
            telefono.setText(datos.getString("telefono")); 
            cargarCargo(cargo);
            cargoUsuario=datos.getString("cargo");
            cargo.setSelectedItem(cargoUsuario); 
            cargarEstado(estado);
            estadoUsuario=datos.getString("estado");
            estado.setSelectedItem(estadoUsuario);
            cargarSede(peticionConsultarSedes(),sede);
            
            if((datos.getString("cargo")).equals("gerente"))
            {
                sede.setSelectedItem("Seleccione ...");
                sede.setEnabled(false);
            }else{
            sedeUsuario=consultarSedeConRestriccion("nombre","codigo_sede",datos.getString("codigo_sede"));
            sede.setSelectedItem(sedeUsuario);
            }
            
            }
           
     }catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null,"Error inesperado: " + ex,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Responsable: Esteban Aguirre Martinez
     * @param datoBuscar
     * @param restriccion
     * @param valorRestriccion
     * @return String
     * Funcion que se encarga retornar un dato de la tabla sedes
     * segun la restriccion y su valor ingresados
     */
    
    public String consultarSedeConRestriccion(String datoBuscar,String restriccion,String valorRestriccion)
    {
        
        if(valorRestriccion.equals(""))
        {
            return "";
        }
        
        String retornar ="";
        String orden = "SELECT "+datoBuscar+" FROM sedes WHERE "+restriccion+" ='"+valorRestriccion+"';";
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al consultar los datos a la tabla sedes: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        try
        {
            respuesta.next();
            retornar = respuesta.getString(datoBuscar);
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al consultar el nombre de la tabla sedes: " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        return retornar;
    }
    
  
        /**
     * Responsable: Esteban Aguirre Martinez
     * @param cedula
     * @return ResultSet
     * Funcion que carga los años en las fechas pertenecientes a una tabla 
     */
    public ResultSet peticionBuscarAñosTabla(String nombre_campo,String tabla)
    {
        String orden = "SELECT DISTINCT EXTRACT(YEAR FROM "+nombre_campo+") FROM "+tabla+";";
        try
        {
            //Se hace la conexion a la base de datos
            Connection conexion = fachada.conectar();
            //Se ejecuta la instruccion SQL
            instruccion = conexion.createStatement();
            //Se obtienen los datos devueltos de la consulta 
            respuesta = instruccion.executeQuery(orden);
            //Se cierra la conexion con la base de datos
            fachada.cerrarConexion(conexion);
            
        }catch(SQLException sqle)
        {
            JOptionPane.showMessageDialog(null,"Error al consultar la informacion de la tabla ordenes de trabajo " + sqle,
                    "Error",JOptionPane.ERROR_MESSAGE);
        }catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null,"No es posible comunicarse con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    
   /**
     * Responsable: Jhon Frederick Mena R
     * @param codigo
     * @param nombre
     * @param direccion
     * @param telefono
     * @param ciudad 
     * @param cedula_gerente
     * @return boolean
     * Funcion que registra una sede en la base de datos
     */
    public boolean peticionAgregarSede(String codigo,String nombre,String direccion,String telefono,String ciudad,String cedula_gerente)
    {
        String instruccionSQL = "INSERT INTO sedes VALUES ('" + codigo + "','" + telefono + "','" + nombre + "','" + direccion + "','" + ciudad + "','" + cedula_gerente + "');";
        //Permite saber si la consulta SQL se hizo con exito
        boolean exitoso = false;
       
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            exitoso = !instruccion.execute(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException sqlex)
        {
            fachada.cerrarConexion(conexion);
            if(sqlex.getErrorCode() == 0)
            {
                JOptionPane.showMessageDialog(null,"Ya se tiene una sede con el codigo: " + codigo ,
                    "Mensaje",JOptionPane.WARNING_MESSAGE);
            }else
            {
                JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL: " +sqlex,
                        "Mensaje",JOptionPane.ERROR_MESSAGE);
            }
            
        }
         return exitoso;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Funcion que retorna solamente la cedula de los gerentes activos
     * @return ResultSet
     */
   public ResultSet peticionCedulaGerente()
    {
        String instruccionSQL = "SELECT cedula_empleado FROM empleados WHERE estado = 'activo' AND cargo = 'gerente';";
        
        Connection conexion = fachada.conectar();
        
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT 
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException ex) 
        {
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + ex,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }    
   
   /**
    * Rsponsable:Jhon Frederick Mena R
    * Metodo que retorna 
    * @param nombreSede 
    * @return ResultSet
    */
   public ResultSet peticionInformacionSedes(String nombreSede)
   {
       String instruccionSQL = "SELECT nombre,direccion,telefono,ciudad "
               + "FROM sedes WHERE nombre = '" + nombreSede + "';";
       Connection conexion = fachada.conectar();
       try
       {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT 
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
           
       }catch(SQLException sqlex)
       {
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
       }
       return respuesta;
   }
   
   /**
    * Responsable: Jhon Frederick Mena R
    * Metodo que actualiza la informacion de una sede en especifico
    * @param telefono
    * @param nombre
    * @param direccion
    * @param ciudad 
    * @param sede
    * @return boolean
    */
   public boolean  peticionActualizarSede(String telefono, String nombre, String direccion, String ciudad,String sede)
   {
       String instruccionSQL = "UPDATE sedes SET telefono = '" + telefono + "',nombre = '" + nombre+ "',direccion = '" + direccion + "',ciudad ='" + ciudad 
               + "' WHERE nombre = '" + sede +"';";
       //Permite saber si la consulta SQL se hizo con exito
       boolean exitoso = false;
       Connection conexion = fachada.conectar();
       try
       {
           //Permite enviar instrucciones a la BD
           instruccion = conexion.createStatement();
           //Se ejecuta la instruccion SQL de tipo UPDATE 
           exitoso = !instruccion.execute(instruccionSQL);
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           
           
       }catch(SQLException sqlex)
       {
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
       }
       return exitoso;
       
   }
      
   /**
    * Responsable: Jhon Frederick Mena R
    * Metodo que retorna toda los datos de la tabla piezas_pertenecen de la BD
    * @param codSede
    * @return ResultSet
    */
   public ResultSet peticionConsultarPiezas(String codSede)
   {
       String instruccionSQL = "SELECT codigo_pieza, nombre, marca, cantidad, precio FROM "
               + "piezas_pertenecen  NATURAL JOIN piezas WHERE codigo_sede = '" + codSede +"' ORDER BY codigo_pieza;";
       
       Connection conexion = fachada.conectar();
       
       try
       {
           //Permite enviar instrucciones a la BD
           instruccion = conexion.createStatement();
           //Se ejecuta la instruccion SQL de tipo SELECT 
           respuesta = instruccion.executeQuery(instruccionSQL);
           //Permite cerra la conexion
           fachada.cerrarConexion(conexion);
       }catch(SQLException sqlex)
       {
           //Permite cerra la conexion
           fachada.cerrarConexion(conexion);
           JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
       }
       
       return respuesta;
   }
   
   /**
    * Responsable: Jhon Frederick Mena R
    * Metodo que almacena la orden de trabajo en la base de datos
    * @param ced_cliente
    * @param ced_empleado
    * @param nomb_cliente
    * @param descripcion
    * @param placa
    * @param modelo
    * @param marca
    * @param fecha 
    * @param valorTotal
    * @param estado 
    */ 
   public int peticionOrdenTrabajo(String ced_cliente,String ced_empleado,String nomb_cliente,String descripcion,
           String placa,String modelo,String marca,String fecha,float valorTotal,String estado)
   {
       String instruccionSQL = "INSERT INTO ordenes_de_trabajo (cedula_cliente,cedula_empleado,"
               + "nombre_cliente,descripcion,placa,modelo,marca,fecha_orden,valor_total,estado)"
               + " VALUES ('" + ced_cliente + "','" + ced_empleado + "','" + nomb_cliente + "','"
               + descripcion + "','" + placa + "','" + modelo + "','"
               + marca + "','" + fecha + "'," + valorTotal + ",'" + estado + "');"; 
       System.out.println(instruccionSQL);
       
       Connection conexion = fachada.conectar();
       int exitoso = 0;
       try
       {
           //Permite enviar instrucciones a la BD
           instruccion = conexion.createStatement();
           //Se ejecuta la instruccion SQL de tipo INSET 
            exitoso = instruccion.executeUpdate(instruccionSQL);
           //Permite cerra la conexion
           fachada.cerrarConexion(conexion);
       }catch(SQLException sqlex)
       {
           //Permite cerra la conexion
           fachada.cerrarConexion(conexion);
           JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
       }
       return exitoso;
   }
   
   /**
    * Responsable: Jhon Frederick Mena R.
    * Metodo que retorna el nombre, cargo y sede a la que pertenece un usuario determinado
    * @param cedEmpleado
    * @return Resulset
    */
   public ResultSet peticionInformacionInicio(String cedEmpleado)
   {
       String instruccionSQL = "SELECT empleados.nombre,apellido,cargo,sedes.nombre FROM empleados INNER JOIN sedes "
               + "ON (empleados.codigo_sede = sedes.codigo_sede) WHERE empleados.cedula_empleado = '" + cedEmpleado + "';";
       
       Connection conexion = fachada.conectar();
       try
       {
           //Permite enviar instrucciones a la BD
           instruccion = conexion.createStatement();
           //Se ejecuta la instruccion SQL de tipo SELECT 
           respuesta = instruccion.executeQuery(instruccionSQL);
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           //Permite cerra la conexion
           fachada.cerrarConexion(conexion);
       }catch(SQLException sqlex)
       {
           //Permite cerra la conexion
           fachada.cerrarConexion(conexion);
           JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
       }
       
       return respuesta;
       
   }
   
   /**
    * Responsable: Jhon Frederick Mena R
    * Metodo que reduce de una pieza, la cantidad de elementos que que hay en una sede
    * @param valor
    * @param codPieza
    * @param codSede
    * @return int
    */
   public int peticionReducirCantidadPieza(int valor, int codPieza,String codSede)
   {
       String instruccionSQL = "UPDATE piezas_pertenecen SET cantidad = (SELECT cantidad FROM piezas_pertenecen "
               + "WHERE codigo_pieza = " + codPieza + " AND codigo_sede = '"+ codSede +"') - " + valor
               + "WHERE codigo_pieza = " + codPieza + " AND codigo_sede = '" + codSede + "';";
       
       Connection conexion = fachada.conectar();
       int exitoso = 0;
       try
       {
           //Permite enviar instrucciones a la BD
           instruccion = conexion.createStatement();
           //Se ejecuta la instruccion SQL de tipo SELECT 
           exitoso = instruccion.executeUpdate(instruccionSQL);
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
       }catch(SQLException sqlex)
       {
           //Permite cerra la conexion
           fachada.cerrarConexion(conexion);
           JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
       }
       
       return exitoso;
   }
   
        /**
     * Resposnable: Jhon Frederick Mena R
     * Método que retorna el codigo_vehiculo, marca, el modelo, color, precio y
     * cantidad de los vehiculos de una sede en particular.
     * @param codSede
     * @return ResultSet 
     */
    public ResultSet peticionConsultarVehiculos(String codSede)
    {

        String InstruccionSQL = "SELECT  codigo_vehiculo, marca, modelo, color,cantidad, precio FROM vehiculos "
                + "NATURAL JOIN vehiculos_pertenecen WHERE codigo_sede = '" + codSede + "' ORDER BY codigo_vehiculo;";

        Connection conexion = fachada.conectar();

        try 
        {

            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT
            respuesta = instruccion.executeQuery(InstruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;

    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que permite almacenar una cotizacion en la base de datos
     * @param nombCliente
     * @param cedCliente
     * @param cedEmpleado
     * @param fecha
     * @param totalCot
     * @return int
     */
    public int peticionGuardarCotizacion(String nombCliente,String cedCliente,String cedEmpleado,String fecha,float totalCot)
    {
        String instruccionSQL = "INSERT INTO cotizaciones (nombre_cliente,cedula_cliente,cedula_empleado,fecha,total_cotizacion) "
                + "VALUES ('" + nombCliente + "','" + cedCliente + "','" + cedEmpleado + "','" + fecha + "'," + totalCot + ");";
        
        Connection conexion = fachada.conectar();
        int exitoso = 0;      
        try 
        {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            exitoso = instruccion.executeUpdate(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return exitoso;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que almacena almacena los codigos de los vehiculos que fueron cotizados en una determinada
     * cotizacion
     * @param codVeh
     * @param cantidad
     * @param total_veh_cot
     * @return int
     */
    public int peticionGuardarVehiculosCotizados(int codVeh, int cantidad, float total_veh_cot)
    {
        String instruccionSQL = "INSERT INTO vehiculos_cotizados VALUES ((SELECT MAX(codigo_cotizacion) FROM cotizaciones)," + codVeh
                + "," + cantidad + "," + total_veh_cot + ");";
        Connection conexion = fachada.conectar();
        int exitoso = 0;      
        try 
        {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            exitoso = instruccion.executeUpdate(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return exitoso;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que almacena almacena los codigos de las piezas que fueron usadas en una determinada
     * orden de trabajo
     * @param codPieza
     * @param cantidad
     * @param total_Precio_Pieza
     * @return int
     */
    public int peticionGuardarPiezasOrden(int codPieza,int cantidad,float total_Precio_Pieza)
    {
       String instruccionSQL = "INSERT INTO piezas_orden_trabajo VALUES ((SELECT MAX(codigo_orden) FROM ordenes_de_trabajo)," + codPieza
                + "," + cantidad + "," + total_Precio_Pieza + ");";
        Connection conexion = fachada.conectar();
        int exitoso = 0; 
        try 
        {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            exitoso = instruccion.executeUpdate(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return exitoso;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que devuelve las ordenes que tengan un determinado estado
     * @param estado
     * @param nombreSede 
     * @return ResultSet
     */
    public ResultSet peticionConsultarEstadoOrdenes(String estado,String nombreSede)
    {
        String instruccionSQL = "SELECT codigo_orden,OT.cedula_empleado,descripcion,valor_total,OT.estado FROM ordenes_de_trabajo OT "
                + "INNER JOIN empleados ON (OT.cedula_empleado = empleados.cedula_empleado)"
                + " INNER JOIN sedes ON (empleados.codigo_sede =  sedes.codigo_sede) "
                + "WHERE OT.estado = '" + estado + "' AND sedes.nombre  = '" + nombreSede + "';";
        Connection conexion = fachada.conectar();

        try 
        {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que permite modificar el estado de la orden de trabajo
     * @param estado
     * @param codOrden
     * @return int
     */
    public int peticionModificarEstadoOrden (String estado,int codOrden)
    {
        String instruccionSQL = "UPDATE ordenes_de_trabajo SET estado = '" + estado +"'"
                + "WHERE codigo_orden = " + codOrden +";";
        Connection conexion = fachada.conectar();
        int exitoso = 0; 
        try 
        {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo UPDATE
            exitoso = instruccion.executeUpdate(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return exitoso;
    }

    /**
     * Respomsable: Jhon Frederick Mena R
     * Metodo que devuelve la consulta con los datos que se mostraran en el reporte correspondiente de las
     * cotizaciones segun un mes y una sede en especifico
     * @param nomb_sede
     * @param mes
     * @param año
     * @return ResultSet
     */
    public ResultSet peticionReporteCotizaciones(String nomb_sede,int mes, int año)
    {
            
        String instruccionSQL = "SELECT  VC.codigo_vehiculo,marca,modelo,color,cantidad "
                + "FROM vehiculos_cotizados VC NATURAL JOIN cotizaciones NATURAL JOIN empleados "
                + "INNER JOIN sedes ON (sedes.codigo_sede = empleados.codigo_sede) INNER JOIN vehiculos "
                + "ON (VC.codigo_vehiculo = vehiculos.codigo_vehiculo) "
                + "WHERE sedes.nombre = '" + nomb_sede +"' AND EXTRACT(MONTH FROM fecha) = " + mes +" "
                + "AND EXTRACT(YEAR FROM fecha) = " + año + ";";
        Connection conexion = fachada.conectar();
        try 
        {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que devuelve la consulta con los datos que se mostraran en el reporte correspondiente de los
     * usuarios de una sede en especifico
     * @param nombreSede
     * @return ResultSet
     */
    public ResultSet peticionReporteUsuariosSede(String nombreSede)
    {
       String instruccionSQL = "SELECT empleados.cedula_empleado, empleados.nombre,apellido,cargo,estado,sedes.nombre "
               + "FROM empleados INNER JOIN sedes ON (empleados.codigo_sede = sedes.codigo_sede) "
               + "WHERE sedes.nombre = '" + nombreSede + "';";
       Connection conexion = fachada.conectar();
       try
       {
           //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
           
       }catch(SQLException sqlex)
       {
           //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
       }
       return respuesta;
    }

    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que devuelve la consulta con los datos que se mostraran en el reporte correspondiente de los
     * usuarios de un cargo en especifico
     * @param cargo
     * @return ResultSet
     */
    public ResultSet peticionReporteUsuariosCargo(String cargo)
    {
        String instruccionSQL = "SELECT empleados.cedula_empleado, nombre,apellido,cargo,estado,nombre "
               + "FROM empleados WHERE cargo = '" + cargo + "';";
       Connection conexion = fachada.conectar();
       try
       {
           //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
           
       }catch(SQLException sqlex)
       {
           //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
       }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que devuelve la consulta con los datos que se mostraran en el reporte correspondiente a los
     * vehiculos que se han vendido en total por un mes especifico
     * @param mes
     * @param año
     * @return ResultSet
     */
    public ResultSet peticionTotalVehiculosVendidos (int mes,int año)
    {
        String instruccionSQL = "SELECT DISTINCT vehiculos.codigo_vehiculo, marca, modelo,color,subconsulta.cantidad "
              + "FROM (SELECT codigo_vehiculo, SUM(cantidad) cantidad FROM vehiculos_vendidos GROUP BY codigo_vehiculo) subconsulta "
              + "INNER JOIN vehiculos ON (vehiculos.codigo_vehiculo =  subconsulta.codigo_vehiculo) "
              + "INNER JOIN vehiculos_vendidos ON (vehiculos_vendidos.codigo_vehiculo =  vehiculos.codigo_vehiculo) "
              + "INNER JOIN ventas ON (vehiculos_vendidos.codigo_ventas = ventas.codigo_ventas) "
              + "WHERE EXTRACT (MONTH FROM fecha) = " + mes + " AND EXTRACT (YEAR FROM fecha) = " + año + ";";
        Connection conexion = fachada.conectar();
        try {
           //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
           
       }catch(SQLException sqlex)
       {
           //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
       }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna informacion necesaria para generar un formato pdf con informacion
     * relevante para ser almacenada
     * @return ResultSet
     */
    public ResultSet peticionGenerarCotizacion()
    {
        String instruccionSQL = "SELECT nombre_cliente,cedula_cliente,fecha,codigo_vehiculo,marca,modelo,color,"
                + "cantidad,total_veh_cot,total_cotizacion FROM vehiculos "
                + "NATURAL JOIN vehiculos_cotizados NATURAL JOIN cotizaciones "
                + "WHERE codigo_cotizacion = (SELECT MAX(codigo_cotizacion) "
                + "FROM cotizaciones);";
        Connection conexion = fachada.conectar();
        try {
           //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
           
       }catch(SQLException sqlex)
       {
           //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
       }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna informacion necesaria para generar un formato teniendo
     * en cuenta el codigo de una cotización
     * @param codigoCot
     * @return ResultSet
     */
    public ResultSet peticionGenerarCotizacion(String codigoCot)
    {
        String instruccionSQL = "SELECT nombre_cliente,cedula_cliente,fecha,codigo_vehiculo,marca,modelo,color,"
                + "cantidad,total_veh_cot,total_cotizacion FROM vehiculos "
                + "NATURAL JOIN vehiculos_cotizados NATURAL JOIN cotizaciones "
                + "WHERE codigo_cotizacion = '" + codigoCot + "';";
        Connection conexion = fachada.conectar();
        try {
           //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
           
       }catch(SQLException sqlex)
       {
           //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
       }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna las ventas realizadas en una determinada sede
     * @param nombreSede
     * @return ResultSet
     */
    public ResultSet peticionConsultarVentas(String nombreSede)
    {
        String instruccionSQL = "SELECT codigo_ventas,cedula_cliente,nombre_cliente,fecha,metodo_de_pago,precio_total_venta "
                + "FROM ventas NATURAL JOIN empleados INNER JOIN sedes ON "
                + "(sedes.codigo_sede = empleados.codigo_sede) WHERE sedes.nombre = '" + nombreSede + "';";
        Connection conexion = fachada.conectar();
        try {
           //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    /**
     * Responsable: Jhon Frederick Mena R
     * Metodo que retorna las cotizaciones realizadas en una determinada sede
     * @param nombreSede
     * @return ResultSet
     */
    public ResultSet peticionConsultarCotizaciones(String nombreSede)
    {
        String instruccionSQL = "SELECT codigo_cotizacion,cedula_cliente,nombre_cliente,fecha,total_cotizacion "
                + "FROM cotizaciones NATURAL JOIN empleados INNER JOIN sedes "
                + "ON (sedes.codigo_sede = empleados.codigo_sede) "
                + "WHERE sedes.nombre = '" + nombreSede + "';";
        Connection conexion = fachada.conectar();
        try {
           //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo SELECT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) 
        {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL\n" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
      
   /**
    * Responsable: Sara Catalina Muñoz H.
    * @param cedEmpleado
    * @return ResultSet
    * Método que saca de la base de datos el nombre y el cargo de un empleado.
    * Usado en la interfaz del Gerente (unicamente)
    */
   public ResultSet peticionInformacionInicioGerente(String cedEmpleado)
   {
       String instruccionSQL = "SELECT empleados.nombre,apellido,cargo FROM empleados "
               + "WHERE empleados.cedula_empleado = '" + cedEmpleado + "';";
       Connection conexion = fachada.conectar();
       try
       {
           //Permite enviar instrucciones a la BD
           instruccion = conexion.createStatement();
           //Se ejecuta la instruccion SQL de tipo SELECT 
           respuesta = instruccion.executeQuery(instruccionSQL);
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           //Permite cerra la conexion
           fachada.cerrarConexion(conexion);
       }catch(SQLException sqlex)
       {
           //Permite cerra la conexion
           fachada.cerrarConexion(conexion);
           JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
       }
       
       return respuesta;
       
   }
   
   
   
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @return ResultSet
     * Método que consulta las sedes existentes en el sistema y retorna sus nombres
     * en un ResultSet
     */
    public ResultSet peticionConsultarSedes(){
        String instruccionSQL = "SELECT nombre,codigo_sede FROM sedes;";
        Connection conexion = fachada.conectar();
         try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
         
         return respuesta;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @return ResultSet
     * Método que selecciona la informacion pertinende de los empleados gerentes
     * y se retorna en un ResultSet
     */
    public ResultSet peticionConsultarGerente(){
         //Se define la instruccion SQL
         String instruccionSQL = "SELECT nombre, apellido, cedula_empleado, email, telefono, direccion, cargo, estado FROM empleados WHERE cargo = 'gerente';";
         Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
            
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @return ResultSet
     * Método que consulta la informacion respectiva de los empleados que son vendedores 
     * y se retorna en un Resultset
     */
    public ResultSet peticionConsultarVendedor(){
        //Se define la instruccion SQL
         String instruccionSQL = "SELECT nombre, apellido, cedula_empleado,email, telefono, direccion, cargo, estado FROM empleados WHERE cargo = 'vendedor';";
         Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
            
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @return ResultSet
     * Método que retorna la infromacion correspondiente de los empleado sjefes taller de la tabla empleados.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet peticionConsultarJefeTaller(){
        
         String instruccionSQL = "SELECT nombre, apellido, cedula_empleado,email, telefono, direccion, cargo, estado FROM empleados WHERE cargo = 'jefe taller';";
         Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
            
    }
    
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param cedula
     * @return ResultSet
     * Método que toma la informacion respectiva de un usuario segun su cedula.
     * La infromación se retorna en un ResultSet
     */
    public ResultSet peticionConsultarUsuarioCedula(String cedula){
         String instruccionSQL = "SELECT nombre, apellido, cedula_empleado,email, telefono, direccion, cargo, estado FROM empleados WHERE cedula_empleado = '"+cedula+"';";
         
         System.out.println(instruccionSQL);
         Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }

    /**
     * Resposnable: Sara Catalina Muñoz H.
     * @param sede
     * @return ResultSet
     * Método que retorna la informacion de los usuarios que trabajan en una sede en particular.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet peticionConsultarUsuarioSede(String sede){
        String instruccionSQL = "SELECT nombre, apellido, cedula_empleado, email,telefono, direccion, cargo, estado FROM empleados NATURAL JOIN sedes as S(codigo_sede, telefono_sede, nombre_sede, dirección_sede, ciudad, cedula_em) WHERE S.nombre_sede = '"+sede+"';";
        System.out.println(instruccionSQL);
        System.out.println("-----");
        
        Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    /**
     * Resposnable: Sara Catalina Muñoz H.
     * @param marca
     * @param modelo
     * @param color
     * @param precio
     * @return boolean
     * Método que hace la peticion de insertar un nuevo registro en la tabla vehiculos
     * Se retorna un booleano confirmand el exito de la operacion
     */
    public boolean peticionRegistrarAutomovil(String marca, String modelo, String color, String precio){
       String instruccionSQL = "INSERT INTO vehiculos (marca, modelo, color, precio) VALUES ('"+marca+"', '"+modelo+"', '"+color+"' ,"+precio+");";
       boolean exitoso = false;
       Connection conexion = fachada.conectar();
       try
       {
           //Permite enviar instrucciones a la BD
           instruccion = conexion.createStatement();
           //Se ejecuta la instruccion SQL de tipo UPDATE 
           exitoso = !instruccion.execute(instruccionSQL);
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           
           
       }catch(SQLException sqlex)
       {
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
       }
       return exitoso;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @return ResultSet
     * Método que consulta las marcas de vehiculos existentes en el sistema
     * y se retornan en un ResultSet
     */
    public ResultSet peticionConsultarMarcas(){
        String instruccionSQL = "SELECT DISTINCT marca from vehiculos;";
        
        Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    /**
     * Resposnable: Sara Catalina Muñoz H.
     * @param marca
     * @return ResultSet
     * Método que consulta los modelos de una marca de vehiculos en particular
     */
    public ResultSet peticionConsultarModelos(String marca){
         String instruccionSQL = "SELECT DISTINCT modelo from vehiculos WHERE marca = '"+marca+"';";
        
        Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param marca
     * @param modelo
     * @return ResultSet
     * Método que retorna una lista de colores segun una marca y modelo ve vehiculo
     */
    public ResultSet peticionConsultarColores(String marca, String modelo){
         String instruccionSQL = "SELECT DISTINCT color from vehiculos WHERE marca = '"+marca+"' AND modelo = '"+modelo+"';";
        
        Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param nombre
     * @return ResultSet
     * Método que selecciona el codigo de la sede segun su nombre
     */
    public ResultSet peticionCodigoSede(String nombre){
        String instruccionSQL = "SELECT codigo_sede from sedes where nombre='"+nombre+"';";
        System.out.println(instruccionSQL);
        Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param codigo
     * @param sede
     * @return ResultSet
     * Método que retorna la cantidad de vehiculos en una sede en especifico
     */
    public ResultSet peticionCantidadVehiculos(String codigo, String sede){
        String instruccionSQL = "SELECT cantidad from vehiculos_pertenecen where codigo_vehiculo='"+codigo+"' AND codigo_sede='"+sede+"';";
        System.out.println(instruccionSQL);
        Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param codigo
     * @param sede
     * @return boolean
     * Método que actualiza un registro en la tabla vehiculos_pertenecen
     */
    public boolean peticionAgregarVehiculoExiste(String codigo, String sede, String cantidad){
        String instruccionSQL ="UPDATE vehiculos_pertenecen SET cantidad = "+cantidad+" WHERE codigo_vehiculo='"+codigo+"'AND codigo_sede ='"+sede+"';";
        System.out.println(instruccionSQL);
        boolean exitoso = false;
       Connection conexion = fachada.conectar();
       try
       {
           //Permite enviar instrucciones a la BD
           instruccion = conexion.createStatement();
           //Se ejecuta la instruccion SQL de tipo UPDATE 
           exitoso = !instruccion.execute(instruccionSQL);
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           
           
       }catch(SQLException sqlex)
       {
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
       }
       return exitoso;
    }
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param marca
     * @param modelo
     * @param color
     * @param sede
     * @param cantidad
     * @return boolean
     * Método que inserta un resgitro en la tabla vehiculos_pertenecen
     */
    public boolean peticionAgregarVehiculoNoExiste(String codigo, String sede, String cantidad){
       String instruccionSQL ="INSERT INTO vehiculos_pertenecen VALUES ('"+codigo+"', '"+sede+"', "+cantidad+");";
        System.out.println(instruccionSQL);
        boolean exitoso = false;
       Connection conexion = fachada.conectar();
       try
       {
           //Permite enviar instrucciones a la BD
           instruccion = conexion.createStatement();
           //Se ejecuta la instruccion SQL de tipo UPDATE 
           exitoso = !instruccion.execute(instruccionSQL);
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           
           
       }catch(SQLException sqlex)
       {
           //Se cierra la conexion
           fachada.cerrarConexion(conexion);
           JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
       }
       return exitoso;
    }
    
   /**
    * Responsable: Sara Catalina Muñoz H.
    * @param vehiculo
    * @param sede
    * @return ResultSet
    * Método que verifica si hay existencias de un modelo en una sede en especifico
    */
    public ResultSet peticionVerificarVehiculoEnSede(String vehiculo, String sede){
        String instruccionSQL = "SELECT COUNT(*) FROM vehiculos_pertenecen WHERE codigo_vehiculo ='"+vehiculo+"' AND codigo_sede='"+sede+"';";
        System.out.println(instruccionSQL);
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    }

    /**
     * Resposnable: Sara Catalina Muñoz H.
     * @param marca
     * @param modelo
     * @param color
     * @return ResultSet
     * Método que selecciona el codigo de un carro segun su marca, modelo y color.
     */
    public ResultSet peticionVerificarAnadirV(String marca, String modelo, String color){
        String instruccionSQL ="SELECT codigo_vehiculo from vehiculos where marca='"+marca+"' AND modelo='"+modelo+
              "' AND color='"+color+"';";
        System.out.println(instruccionSQL);
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    }
    
    
    /**
     * Responsable: Sara Catalina Muñoz H.
     * @param marca
     * @param modelo
     * @param precio
     * @param color
     * @return ResultSet
     * Método que busca una referencia de vehiculo segun su marca, modelo y color
     */
    public ResultSet peticionVerificarModelo(String marca, String modelo, String color){
      String instruccionSQL ="SELECT COUNT(*) from vehiculos where marca='"+marca+"' AND modelo='"+modelo+
              "' AND color='"+color+"';";
        System.out.println(instruccionSQL);
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            
        } catch (SQLException sqlex){          
        
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;         
    }
    
    /**
     * Resposnable: Sara Catalina Muñoz H.
     * @return ResultSet
     * Método que selecciona la informacion de las sedes
     */
    public ResultSet peticionReporteSedes(){
      String instruccionSQL ="SELECT nombre, direccion, ciudad FROM sedes;";
        System.out.println(instruccionSQL);
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            
        } catch (SQLException sqlex){          
        
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;         
    }
    
    /**
     * Resposnable: Sara Catalina Muñoz H.
     * @return ResultSet
     * Método que selecciona la informacion de los vehiculos para crear el respectivo reporte
     */
    public ResultSet peticionReporteVehiculos(String codigo){
      String instruccionSQL ="select codigo_vehiculo, marca, modelo, color ,precio, cantidad from vehiculos natural join vehiculos_pertenecen where codigo_sede = '"+codigo+"';";
        System.out.println(instruccionSQL);
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            
        } catch (SQLException sqlex){          
        
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;         
    }
    
    
      
    /**
  * Responsable: Sara Catalina Muñoz H.
  * @return ResultSet
  * Método que selecciona la informacion de las sedes con su respectivas ventas para realizar el reporte grafico
  */   
 public ResultSet peticionGraficoVentas(String mes){
      String instruccionSQL =" select sedes.nombre, SUM(ventas.precio_total_venta) from ventas INNER JOIN empleados ON (ventas.cedula_empleado = empleados.cedula_empleado)"
              +"INNER JOIN sedes ON (empleados.codigo_sede = sedes.codigo_sede) WHERE EXTRACT(month FROM ventas.fecha) = '"+mes+"' GROUP BY sedes.nombre;";
        System.out.println(instruccionSQL);
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(instruccionSQL);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            
        } catch (SQLException sqlex){          
        
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;         
    }
    
    
    
    
     /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param usuario
     * @return ResultSet
     * Método que retorna el codigo de sede y cedula del empleado un empleado en especifico.
     * La informacion se retorna en un ResultSet
     */
      public ResultSet sedeYcedulaEmpleado(String usuario){
    
        String consulta = "SELECT codigo_sede, cedula_empleado FROM empleados WHERE usuario ='"+usuario+"';";
        String resultado="";
        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    }
      
     /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param sede
     * @return ResultSet
     * Método que retorna la marca, el modelo, color, precio y cantidad de los vehiculos de una sede en particular.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet peticionConsultarInventarioVehiculos(String sede){

        String consulta = "SELECT  marca, modelo, color, precio, cantidad FROM vehiculos NATURAL JOIN vehiculos_pertenecen"
             + " NATURAL JOIN sedes WHERE nombre = '"+sede+"';";

        Connection conexion = fachada.conectar();
        
        try {
           
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL" ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return respuesta;
    
    }
    
     /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param combo
     * @param sede
     * @return ResultSet
     * Método que retorna la marca de un vehículo en especifico perteneciente a una sede en particular.
     * La informacion se retorna en un ResultSet
     */
     public ResultSet cargarMarcas(JComboBox combo, String sede){
    
    String consulta = "SELECT DISTINCT marca FROM vehiculos NATURAL JOIN vehiculos_pertenecen"
                    + " WHERE codigo_sede ='"+sede+"';";
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            while(respuesta.next()){
                
                        System.out.println(respuesta.getArray(1).toString());
                        combo.addItem(respuesta.getArray(1).toString());
                                                                    
                    }          
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    
    }
    
     /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param combo
     * @param sede
     * @param marca     
     * @return ResultSet
     * Método que retorna el modelo de un vehículo con una marca
     * en específico perteneciente a una sede en particular.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet cargarModelos(JComboBox combo, String sede, String marca){
    
    String consulta = "SELECT DISTINCT modelo FROM vehiculos NATURAL JOIN vehiculos_pertenecen "
                        + "WHERE codigo_sede ='"+sede+"' AND marca ='"+marca+"' ;";
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            while(respuesta.next()){
                
                        System.out.println(respuesta.getArray(1).toString());
                        combo.addItem(respuesta.getArray(1).toString());
                                                                    
                    }          
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    
    }
    
       /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param combo
     * @param marca
     * @param modelo     
     * @param sede     
     * @return ResultSet
     * Método que retorna el color de un vehículo con una marca y modelo
     * en específico perteneciente a una sede en particular.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet cargarColores(JComboBox combo, String marca, String modelo, String sede){
    
    String consulta = "SELECT color FROM vehiculos NATURAL JOIN vehiculos_pertenecen WHERE"
                      + " codigo_sede ='"+sede+"' AND marca ='"+marca+"' AND modelo = '"+modelo+"';";
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
            while(respuesta.next()){
                
                        System.out.println(respuesta.getArray(1).toString());
                        combo.addItem(respuesta.getArray(1).toString());
                                                                    
                    }          
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    
    }
    
    
     /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param marca
     * @param modelo
     * @param sede     
     * @param color     
     * @return ResultSet
     * Método que retorna el codigo y precio del vehículos con una marca, modelo y color
     * en específico perteneciente a una sede en particular.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet codigoYPrecioVehiculo(String marca, String modelo, String sede, String color){
    
    
     String consulta = "SELECT precio, codigo_vehiculo FROM vehiculos NATURAL JOIN vehiculos_pertenecen"
                          + " WHERE codigo_sede ='"+sede+"' AND marca ='"+marca+"' "
                          + "AND modelo = '"+modelo+"' AND color = '"+color+"';";
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    
    }
    
    /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param vehiculo
     * @param sede       
     * @return ResultSet
     * Método que retorna la cantidad de un vehículo con un código en especifico
     * y perteneciente a una sede en particular.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet cantidadVehiculo(String vehiculo, String sede){
    
    String consulta = "SELECT cantidad FROM vehiculos_pertenecen WHERE codigo_vehiculo ='"+vehiculo
                        +"' AND codigo_sede ='"+sede+"';";
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    
    
    }  
    
    
     /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param cedula
     * @param telefono
     * @param precio
     * @param metodo_de_pago
     * @param cod_vehiculo       
     * @param cedula_vendedor       
     * @param nombre       
     * @param cantidad
     * @return ResultSet
     * Método registra una venta en la base de datos según los datos ingresados como
     * parámetro. Devuelve un número igual a cero no se registro correctamente o
     * diferente de cero si fue correcto.
     * La informacion se retorna en un int
     */
    public int registrarVenta(String cedula, String telefono, Float precio, String metodo_de_pago,
                                  String cedula_vendedor, String nombre, String fecha){
    
    
    String orden = "INSERT INTO ventas (cedula_cliente, telefono_cliente, precio_total_venta, metodo_de_pago, "
            + "                         fecha, cedula_empleado,"
            + "                         nombre_cliente) "
            + "VALUES('"+cedula+"','"+telefono+"',"+precio+",'"+metodo_de_pago+"','"+fecha
                        +"','"+cedula_vendedor+"','"+nombre+"');";
    int resultado = 0;
    
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return resultado;  
        }
    
    
     /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param cod_vehiculo
     * @param cantidad_vehiculo       
     * @param cantidad       
     * @param codigo_sede       
     * @return ResultSet
     * Método descuenta a la cantidad ingresada como parámetro a la cantidad de vehículos 
     * de la base de datos según el codigo de vehiculo ingresado por parámetro.
     * Devuelve cero si no se desconto satisfactoriamente o un número diferente de cero
     * si se logro descontar satisfactoriamente.
     * La informacion se retorna en un int
     */
    public int descontarVehiculos(String cod_vehiculo, Integer cantidad_vehiculo, Integer cantidad, String codigo_sede){
    int resultado = 0;
    
    String consulta = "UPDATE vehiculos_pertenecen SET cantidad ="+(cantidad_vehiculo-cantidad)+" WHERE codigo_vehiculo = '"+cod_vehiculo+"' "
            + "AND codigo_sede = '"+codigo_sede+"';";
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                    
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
        
        return resultado;
        
    }
    
    
     /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param sede       
     * @return ResultSet
     * Método que retorna el nombre, marca, precio y cantidad de las piezas 
     * pertenecientes a una sede en particular.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet consultarInventarioPiezas(String sede){
     String consulta = "SELECT nombre, marca, precio, cantidad FROM piezas NATURAL JOIN piezas_pertenecen"
             + " NATURAL JOIN sedes sedes(codigo_sede, telefono, nombre_sede, direccion, ciudad, cedula_empleado)"
             + " WHERE nombre_sede = '"+sede+"';";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    }
    
    
     /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param sede       
     * @return ResultSet
     * Método que retorna el nombre, direccion, ciudad y telefono
     * de una sede en particular.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet consultarSede(String sede){
    String consulta = "SELECT nombre, direccion, ciudad, telefono FROM sedes"
             + " WHERE nombre = '"+sede+"';";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    }
    
     /**
     * Resposnable: Annie Paola Muñoz LL.       
     * @return ResultSet
     * Método que el codigo de venta de la ultima venta realizada en el sistema.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet codigoUltimaVenta(){
    String consulta = "SELECT MAX(codigo_ventas) FROM ventas;";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;
    
    }
    
    
    /**
     * Resposnable: Annie Paola Muñoz LL.       
     * @param codigo_venta
     * @param codigo_vehiculo
     * @param cantidad
     * @param precio_total
     * @return ResultSet
     * Método que registra un vehículo como vendido con las caracteristicas
     * ingresadas como parámetros. Devuelve un número igual a cero no se registro correctamente o
     * diferente de cero si fue correcto.
     * La informacion se retorna en un int
     */
    public int registrarVehiculosVenta(String codigo_venta, String codigo_vehiculo, String cantidad, String precio_total){

    String orden = "INSERT INTO vehiculos_vendidos VALUES("+codigo_venta+","+codigo_vehiculo
                   +","+cantidad+","+precio_total+");";
    int resultado = 0;
    
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            resultado = instruccion.executeUpdate(orden);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return resultado;  
        }
    
    
    /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param sede      
     * @param mes      
     * @param año      
     * @return ResultSet    
     * Método que retorna la marca, modelo, color, precio y cantidad de los vehiculos
     * vendidos en una sede , mes y año en especifico.	 
     * La informacion se retorna en un ResultSet
     */
    public ResultSet consultarVentasMes(String sede, String mes, String año){
    String consulta = "SELECT DISTINCT marca, modelo, color, precio, cantidad FROM vehiculos"
            + " NATURAL JOIN(SELECT codigo_vehiculo, SUM(cantidad) cantidad  FROM vehiculos_vendidos"
            + " NATURAL JOIN ventas NATURAL JOIN empleados INNER JOIN  sedes ON (sedes.codigo_sede ="
            + " empleados.codigo_sede) GROUP BY codigo_vehiculo, sedes.nombre, ventas.fecha HAVING sedes.nombre ="
            + " '"+sede+"' AND EXTRACT(month FROM fecha) = "+mes+" AND EXTRACT(year FROM fecha) = "+año+") subconsulta;";
         
    Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion =  conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);
                        
        } catch (SQLException sqlex)
        {
            JOptionPane.showMessageDialog(null,"Error al procesar la instruccion SQL:" + sqlex ,
                    "Mensaje",JOptionPane.ERROR_MESSAGE);
        }
    
        return respuesta;   
    }    
    
     /**
     * Resposnable: Annie Paola Muñoz LL.
     * @param codigo       
     * @return ResultSet
     * Método que retorna la información de una venta en especifico,
     * que se necesita para la factura.
     * La informacion se retorna en un ResultSet
     */
    public ResultSet consultarDetalleVenta(String codigo)
    {
        String consulta = "SELECT codigo_vehiculo,cantidad, nombre_cliente, cedula_cliente, telefono_cliente,"
                + " precio_total_venta, fecha,  marca, modelo, color, precio_total  FROM ventas"
                + " NATURAL JOIN vehiculos_vendidos NATURAL JOIN vehiculos WHERE codigo_ventas = '" + codigo + "';";

        Connection conexion = fachada.conectar();
        try {
            //Permite enviar instrucciones a la BD
            instruccion = conexion.createStatement();
            //Se ejecuta la instruccion SQL de tipo INSERT 
            respuesta = instruccion.executeQuery(consulta);
            //Se cierra la conexion
            fachada.cerrarConexion(conexion);

        } catch (SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Error al procesar la instruccion SQL:" + sqlex,
                    "Mensaje", JOptionPane.ERROR_MESSAGE);
        }

        return respuesta;
    }
    
    
    
}

