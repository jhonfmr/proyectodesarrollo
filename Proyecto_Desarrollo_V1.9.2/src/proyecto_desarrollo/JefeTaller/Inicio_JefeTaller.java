/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_desarrollo.JefeTaller;

import java.awt.Toolkit;
import java.awt.event.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import proyecto_desarrollo.AccesoDatos;
import proyecto_desarrollo.Login;
/**
 *
 * @author sara
 */
public class Inicio_JefeTaller extends javax.swing.JFrame 
{
    ManejadoraJ maneja;
    static String codigoSede;
    static String cedulaEmpleado;
    
    /**
     * Creates new form inicio
     */
    public Inicio_JefeTaller(String codSede,String  cedEmpleado) {
        initComponents();
        codigoSede = codSede;
        cedulaEmpleado = cedEmpleado;
        //Icono de la venta
        Toolkit tool = Toolkit.getDefaultToolkit();
        setIconImage(tool.getImage(getClass().getResource("/img/icono.jpg")));
        
        maneja = new ManejadoraJ();
        //Posicion del componente realtiva a la pantalla, en este caso en el centro
        setLocationRelativeTo(null);
        /* Adicion de las escuchas MouseListener */
        btn_ConsultarInventario.addMouseListener(maneja);
        btn_OrdenesTrabajo.addMouseListener(maneja);
        btn_ConsultarOrdenes.addMouseListener(maneja);
        /* Adicion de las escuchas ActionListener */
        btn_ConsultarInventario.addActionListener(maneja);
        btn_OrdenesTrabajo.addActionListener(maneja);
        btn_ConsultarOrdenes.addActionListener(maneja);
        btn_logoutJT.addActionListener(maneja);
        /* Adicion de las escuchas WindowsListener */
        addWindowListener(maneja);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnl_Principal = new javax.swing.JPanel();
        lbl_cargoGOTJ = new javax.swing.JLabel();
        lbl_sedeGOTJ = new javax.swing.JLabel();
        lbl_nombreUserGOTJ = new javax.swing.JLabel();
        lbl_userImgGOTJ = new javax.swing.JLabel();
        btn_OrdenesTrabajo = new javax.swing.JButton();
        btn_ConsultarInventario = new javax.swing.JButton();
        btn_ConsultarOrdenes = new javax.swing.JButton();
        lbl_Banner = new javax.swing.JLabel();
        btn_logoutJT = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(java.awt.Color.white);
        setMinimumSize(new java.awt.Dimension(680, 540));
        setPreferredSize(new java.awt.Dimension(680, 540));
        setResizable(false);
        getContentPane().setLayout(null);

        pnl_Principal.setBackground(new java.awt.Color(254, 254, 254));
        pnl_Principal.setLayout(null);

        lbl_cargoGOTJ.setForeground(java.awt.Color.white);
        pnl_Principal.add(lbl_cargoGOTJ);
        lbl_cargoGOTJ.setBounds(480, 40, 170, 20);

        lbl_sedeGOTJ.setForeground(java.awt.Color.white);
        pnl_Principal.add(lbl_sedeGOTJ);
        lbl_sedeGOTJ.setBounds(480, 70, 170, 20);

        lbl_nombreUserGOTJ.setForeground(java.awt.Color.white);
        pnl_Principal.add(lbl_nombreUserGOTJ);
        lbl_nombreUserGOTJ.setBounds(480, 10, 170, 20);

        lbl_userImgGOTJ.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/usuario.png"))); // NOI18N
        pnl_Principal.add(lbl_userImgGOTJ);
        lbl_userImgGOTJ.setBounds(400, 10, 60, 70);

        btn_OrdenesTrabajo.setBackground(new java.awt.Color(254, 254, 254));
        btn_OrdenesTrabajo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bOrdenesTrabajo.png"))); // NOI18N
        pnl_Principal.add(btn_OrdenesTrabajo);
        btn_OrdenesTrabajo.setBounds(440, 290, 140, 140);

        btn_ConsultarInventario.setBackground(new java.awt.Color(254, 254, 254));
        btn_ConsultarInventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bConsultarInventario.png"))); // NOI18N
        pnl_Principal.add(btn_ConsultarInventario);
        btn_ConsultarInventario.setBounds(100, 290, 140, 140);

        btn_ConsultarOrdenes.setBackground(new java.awt.Color(254, 254, 254));
        btn_ConsultarOrdenes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/consultarOT2.png"))); // NOI18N
        btn_ConsultarOrdenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ConsultarOrdenesActionPerformed(evt);
            }
        });
        pnl_Principal.add(btn_ConsultarOrdenes);
        btn_ConsultarOrdenes.setBounds(270, 290, 140, 140);

        lbl_Banner.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/final.png"))); // NOI18N
        pnl_Principal.add(lbl_Banner);
        lbl_Banner.setBounds(0, 0, 680, 100);

        btn_logoutJT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/logout.png"))); // NOI18N
        pnl_Principal.add(btn_logoutJT);
        btn_logoutJT.setBounds(30, 120, 50, 50);

        getContentPane().add(pnl_Principal);
        pnl_Principal.setBounds(0, 0, 680, 560);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_ConsultarOrdenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ConsultarOrdenesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_ConsultarOrdenesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Inicio_JefeTaller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Inicio_JefeTaller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Inicio_JefeTaller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Inicio_JefeTaller.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inicio_JefeTaller(codigoSede, cedulaEmpleado).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_ConsultarInventario;
    private javax.swing.JButton btn_ConsultarOrdenes;
    private javax.swing.JButton btn_OrdenesTrabajo;
    private javax.swing.JButton btn_logoutJT;
    private javax.swing.JLabel lbl_Banner;
    private javax.swing.JLabel lbl_cargoGOTJ;
    private javax.swing.JLabel lbl_nombreUserGOTJ;
    private javax.swing.JLabel lbl_sedeGOTJ;
    private javax.swing.JLabel lbl_userImgGOTJ;
    private javax.swing.JPanel pnl_Principal;
    // End of variables declaration//GEN-END:variables

    /*
    Clase interna que maneja cada uno de los eventos de los diferentes componentes
    */
    public class ManejadoraJ implements MouseListener,ActionListener,WindowListener{

        AccesoDatos accesoDatos = new AccesoDatos();
        
        @Override
        public void mouseClicked(MouseEvent e) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void mousePressed(MouseEvent e) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void mouseEntered(MouseEvent e) 
        {
            //Dependiendo del boton se cambia el icono cuando el mouse esta sobre este
            if(e.getSource() == btn_ConsultarInventario)
            {
                btn_ConsultarInventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bConsultarInventario2.png")));
            }else if(e.getSource() == btn_OrdenesTrabajo)
            {
                btn_OrdenesTrabajo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bOrdenesTrabajo2.png")));
            }else if(e.getSource()== btn_ConsultarOrdenes){
                btn_ConsultarOrdenes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/consultarOT.png")));
            }
        }

        @Override
        public void mouseExited(MouseEvent e) 
        {
            //Dependiendo del boton se cambia el icono cuando el mouse esta fuera de este
             if(e.getSource() == btn_ConsultarInventario)
             {
                btn_ConsultarInventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bConsultarInventario.png")));
            }else if(e.getSource() == btn_OrdenesTrabajo)
            {
                btn_OrdenesTrabajo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bOrdenesTrabajo.png")));
            }else if(e.getSource()== btn_ConsultarOrdenes){
                btn_ConsultarOrdenes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/consultarOT2.png")));
            }
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if(e.getSource() == btn_OrdenesTrabajo)
            {
                GestionarOrdenesTrabajoJefe interfaz = new GestionarOrdenesTrabajoJefe(codigoSede,cedulaEmpleado);
                interfaz.setVisible(true);
                dispose();
                
            }else if(e.getSource() == btn_ConsultarInventario)
            {
                ConsultarInventarioJefeTaller interfaz = new ConsultarInventarioJefeTaller(cedulaEmpleado, codigoSede);
                interfaz.setVisible(true);
                dispose();
                
            }else if (e.getSource() == btn_ConsultarOrdenes)
            {
               ConsultarOrdenesTrabajoJefe interfaz = new ConsultarOrdenesTrabajoJefe(codigoSede, cedulaEmpleado);
               interfaz.setVisible(true);
               dispose(); 
            }else if(e.getSource() == btn_logoutJT)
              {
                  int decision = JOptionPane.showConfirmDialog(null, "¿Desea cerrar la sesión?",
                          "Mensaje", JOptionPane.YES_NO_OPTION);
                 
                  if(decision == JOptionPane.YES_OPTION)
                  {
                      Login inicio = new Login();
                      inicio.setVisible(true);
                      dispose();
                  }
                  
              }
        }

        @Override
        public void windowOpened(WindowEvent e)
        {
            //Permitira mostrar la informacion de la parte superior de la ventana
            ResultSet respuesta = accesoDatos.peticionInformacionInicio(cedulaEmpleado);
            try
            {
                respuesta.next();
                lbl_nombreUserGOTJ.setText(respuesta.getString(1).toUpperCase() + " " + respuesta.getString(2).toUpperCase());
                lbl_cargoGOTJ.setText(respuesta.getString(3).toUpperCase());
                lbl_sedeGOTJ.setText(respuesta.getString(4).toUpperCase());
            }catch(SQLException sqlex)
            {
                JOptionPane.showMessageDialog(null,"Error al cargar la informacion de inicio del usuario\n" + sqlex,
                        "mensaje",JOptionPane.WARNING_MESSAGE);
            }
        }
        

        @Override
        public void windowClosing(WindowEvent e) {
        }

        @Override
        public void windowClosed(WindowEvent e) {
        }

        @Override
        public void windowIconified(WindowEvent e) {
        }

        @Override
        public void windowDeiconified(WindowEvent e) {
        }

        @Override
        public void windowActivated(WindowEvent e) {
        }

        @Override
        public void windowDeactivated(WindowEvent e) {
        }
        
    }
}
