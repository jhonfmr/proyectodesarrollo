/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_desarrollo;

import java.sql.*;
import javax.swing.JOptionPane;


public class Fachada {
   String url, usuario, fachada, password;
   Connection conexion;
   Statement instruccion;
   
   //Constructor de la clase Fachada, tiene un url, un usuario y un password predefinidos
   Fachada(){
       url = "jdbc:postgresql://localhost:5432/jhonfmr";
       usuario ="jhonfmr";
       password="jhonfmr";
   }
   
   //Metodo que carga el driver respectivo y ejecuta la conexion con la base de datos
   public Connection conectar(){
       try{
           Class.forName("org.postgresql.Driver");
      System.out.println("Driver cargado");
       }catch(Exception e){
      System.out.println("No se pudo cargar el driver");
       }
       
       try{
           conexion = DriverManager.getConnection(url,usuario, password);
           System.out.println("Conexion abierta");
           return conexion;
       }catch(Exception e){
           System.out.println("No se pudo abrir");
           return null;
       }
       
   }
   
   public void cerrarConexion(Connection conexion)
    {
        try
        {
            //Se cierra la conexion con la base de datos
            conexion.close();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Fallo inesperado en el cierra de la conexion con la base de datos",
                    "Error",JOptionPane.ERROR_MESSAGE);
        }
    }
  
}